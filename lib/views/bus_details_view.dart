import 'dart:convert';
import 'package:platform/platform.dart';

import 'package:citypass_mobile/models/transport_api/schedule_data_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
// import 'package:url_launcher/url_launcher.dart';

import '../models/transport_api/nearby_stops_model.dart';
import '../services/transport_api_config.dart';
import '../widgets/connection_error_card.dart';
import '../widgets/stop_schedule_list_card.dart';
import 'package:android_intent/android_intent.dart';
import '../widgets/view_title.dart';

class BusDetailsView extends StatefulWidget {
  final NearbyStopsModel stop;
  final userCoords;
  final Map<String, dynamic> walkingDetails;

  BusDetailsView({this.stop, this.walkingDetails, this.userCoords});

  @override
  _BusDetailsViewState createState() => _BusDetailsViewState();
}

class _BusDetailsViewState extends State<BusDetailsView> {
  final scheduleCards = [];

  modelScheduleCards(var res) {
    Iterable list = json.decode(res);

    List scheduleModels =
        list.map((model) => ScheduleDataModel.fromJson(model)).toList();

    return scheduleModels;
  }

  _launchGoogleMaps() async {
    String api = "AIzaSyDfi7qjB9hxmijB246jRw_MY8L2YTIuKJs";
    if (new LocalPlatform().isAndroid) {
      final AndroidIntent intent = new AndroidIntent(
          action: 'action_view',
          data: Uri.encodeFull(
              "https://www.google.com/maps/dir/?api=$api&origin=${widget.userCoords['latitude']},${widget.userCoords['longitude']}&destination=${widget.stop.geometry[1]},${widget.userCoords['longitude']}&destination=${widget.stop.geometry[0]}&travelmode=driving&dir_action=navigate"),
          package: 'com.google.android.apps.maps');
      intent.launch();
    } else {}
  }

  Widget renderStopScheduleCards(List scheduleCardList) {
    List<Widget> modeledCards = [];

    for (ScheduleDataModel item in scheduleCardList) {
      modeledCards.add(
        StopScheduleListCard(
          arrivalDataItem: item,
          walkingDetails: widget.walkingDetails,
        ),
      );
    }

    return Column(children: modeledCards);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0, bottom: 0.0),
        child: ListView(
          children: [
            ViewTitle(
              title: "Schedule",
              subtitle: "${widget.stop.name} Stop.",
              context: context,
            ),
            SizedBox(height: 16.0),
            Center(
              child: Card(
                elevation: 16.0,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(22.0),
                ),
                child: Container(
                  margin: EdgeInsets.all(8.0),
                  height: 210.0,
                  width: 270.0,
                  child: Column(
                    children: <Widget>[
                      Text(
                        "Stop Summary",
                        style: TextStyle(
                          color: Colors.red[300],
                          fontWeight: FontWeight.bold,
                          fontSize: 18.0,
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Column(
                        children: <Widget>[
                          ListTile(
                            leading:
                                Icon(Icons.navigation, color: Colors.red[200]),
                            title: Text(
                              "Distance: ${widget.walkingDetails['distance']} KM",
                              style: TextStyle(color: Colors.red[200]),
                            ),
                          ),
                          ListTile(
                            leading: Icon(
                              Icons.directions_walk,
                              color: Colors.red[200],
                            ),
                            title: Text(
                              "Walking time: ${widget.walkingDetails['time']} min",
                              style: TextStyle(color: Colors.red[200]),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          RaisedButton(
                              elevation: 8.0,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(22.0),
                              ),
                              color: Colors.red[300],
                              child: Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.directions,
                                    color: Colors.white,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    "directions",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ],
                              ),
                              onPressed: () {
                                _launchGoogleMaps();
                              }),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(height: 16.0),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 16.0),
                  Text(
                    'Schedule',
                    style: TextStyle(
                      color: Colors.grey[500],
                      fontWeight: FontWeight.w700,
                      fontSize: 24.0,
                    ),
                  ),
                  SizedBox(height: 24.0),
                  FutureBuilder(
                    future: TransportApiHandler.getStopSchedule(
                        stopId: widget.stop.id),
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      if (snapshot.connectionState == ConnectionState.done) {
                        if (snapshot.hasError) {
                          return ConnectionErrorCard();
                        } else {
                          List modeledData = modelScheduleCards(snapshot.data);
                          for (var item in modeledData) {
                            scheduleCards.add(item);
                          }
                          return renderStopScheduleCards(scheduleCards);
                        }
                      } else {
                        return Center(
                            child: CircularProgressIndicator(
                          valueColor: new AlwaysStoppedAnimation<Color>(
                              Colors.red[100]),
                        ));
                      }
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
