import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import './sign_in_view.dart';
import './home_view.dart';

class SplashScreenView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: StreamBuilder<FirebaseUser>(
          stream: FirebaseAuth.instance.onAuthStateChanged,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.active) {
              FirebaseUser user = snapshot.data;

              return HomeView();
            } else {
              return Scaffold(
                body: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            }
          },
        ),
      ),
    );
  }
}

// Widget splashscreen(BuildContext context) {
//   return
// }

// FutureBuilder(
//         future: FirebaseAuth.instance.currentUser(),
//         builder: (BuildContext context, AsyncSnapshot snapshot) {
//           if (snapshot.connectionState == ConnectionState.waiting) {
//             return Container(
//               width: MediaQuery.of(context).size.width,
//               height: MediaQuery.of(context).size.height,
//               color: Colors.red[300],
//               child: Column(
//                 mainAxisAligRnment: MainAxisAlignment.center,
//                 children: <Widget>[
//                   Text(
//                     "CityPass",
//                     style: TextStyle(
//                       fontSize: 48,
//                       fontWeight: FontWeight.w300,
//                       color: Colors.white,
//                     ),
//                   ),
//                   SizedBox(
//                     height: 7.0,
//                   ),
//                   Text(
//                     "Your Commuting Companion",
//                     style: TextStyle(
//                       fontSize: 20,
//                       fontWeight: FontWeight.w300,
//                       color: Colors.white,
//                     ),
//                   ),
//                   SizedBox(
//                     height: 40.0,
//                   ),
//                   CircularProgressIndicator(
//                     backgroundColor: Colors.white,
//                     valueColor:
//                         new AlwaysStoppedAnimation<Color>(Colors.red[100]),
//                   ),
//                   MaterialButton(
//                     onPressed: () => {},
//                   ),
//                 ],
//               ),
//             );
//           } else {
//             if (snapshot.hasData) {
//               return Container();
//             }
//           }
//         },
//       )),
