import 'package:citypass_mobile/blocs/user_location_bloc/user_search_location_bloc.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../store/bottom_navigation_state.dart';
import '../widgets/bottom_navigation_bar.dart';
import '../widgets/google_maps_auto_complete_card.dart';
import '../widgets/view_title.dart';

class TripsView extends StatefulWidget {
  @override
  _TripsViewState createState() => _TripsViewState();
}

class _TripsViewState extends State<TripsView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0, bottom: 0.0),
        child: ListView(
          children: <Widget>[
            ViewTitle(
              title: "Trips",
              subtitle: "All Your Frequent Trips In One Place",
              context: context,
            ),
            SizedBox(height: 16.0),
            Padding(
              padding:
                  EdgeInsets.only(top: 8.0, right: 8.0, left: 8.0, bottom: 0.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  AutoCompleteLocationSearchBar(
                    text: "Departing Near",
                    isFromLocation: true,
                  ),
                  SizedBox(height: 32.0),
                  AutoCompleteLocationSearchBar(
                    text: "Destination",
                    isFromLocation: false,
                  ),
                  SizedBox(height: 20.0),
                  Center(
                    child: StreamBuilder(
                      stream: userSearchLocationBloc.enableSubmit,
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        return RaisedButton.icon(
                          disabledColor: Colors.blueGrey,
                          elevation: 5.0,
                          splashColor: Colors.red[100],
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(32.0),
                          ),
                          color: Colors.red[300],
                          icon: Icon(Icons.search, color: Colors.white),
                          label: Text(
                            "search",
                            style: TextStyle(color: Colors.white),
                          ),
                          onPressed: () {
                            print(snapshot.data);
                          },
                        );
                      },
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: ChangeNotifierProvider<BottomNavPosition>(
        builder: (_) => BottomNavPosition(1),
        child: BottomNavigator(),
      ),
    );
  }
}
