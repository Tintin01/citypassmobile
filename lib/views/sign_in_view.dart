import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_auth_buttons/flutter_auth_buttons.dart';
// import '../blocs/current_user/current_user_bloc.dart';
import '../services/firebase/fire_auth.dart';
import 'package:firebase_auth/firebase_auth.dart';

class SignInView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle.dark.copyWith(
        statusBarColor:
            Color(0xFF101E21), //or set color with: Color(0xFF0000FF)
      ),
    );
    return Scaffold(
      body: SafeArea(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: ExactAssetImage('assets/train.jpg'),
              fit: BoxFit.cover,
            ),
          ),
          padding: EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 80.0,
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    Text(
                      "Welcome To",
                      style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.w300,
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Text(
                      "CityPass",
                      style: TextStyle(
                        fontSize: 40,
                        fontWeight: FontWeight.w300,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 70.0,
              ),
              Center(
                child: AnimatedOpacity(
                  duration: Duration(seconds: 1),
                  opacity: 0.8,
                  child: Card(
                    color: Color(0xFF647D87),
                    elevation: 8.0,
                    child: Container(
                      padding: EdgeInsets.all(16.0),
                      height: 250.0,
                      width: 400.0,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "Sign Up | Login",
                            style: TextStyle(
                              fontSize: 22,
                              color: Colors.white,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                          SizedBox(height: 10.0),
                          Divider(
                            color: Colors.white70,
                          ),
                          SizedBox(height: 20.0),
                          GoogleSignInButton(
                              text: "Continue with Google",
                              onPressed: () {
                                handleGoogleSignIn().then(
                                  (FirebaseUser user) {
                                    Navigator.of(context)
                                        .popAndPushNamed('/home');
                                  },
                                );
                              },
                              borderRadius: 32.0,
                              darkMode: true),
                          SizedBox(height: 20.0),
                          FacebookSignInButton(
                              text: "Continue with Facebook",
                              onPressed: () {},
                              borderRadius: 32.0),
                          SizedBox(height: 20.0),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
