import 'package:flutter/material.dart';
import '../widgets/bottom_navigation_bar.dart';
import 'package:provider/provider.dart';
import '../store/bottom_navigation_state.dart';
import '../widgets/view_title.dart';
import 'package:flutter/services.dart';

class HomeView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Colors.grey[50], //or set color with: Color(0xFF0000FF),
        systemNavigationBarColor: Colors.grey[50],
      ),
    );
    return Scaffold(
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ViewTitle(
              title: "Home",
              subtitle: "All your frequent commutes in one place.",
            ),
            Center(
              child: Text(
                "Coming Soon-ish",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 24.0,
                    color: Colors.blueGrey),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: ChangeNotifierProvider<BottomNavPosition>(
        builder: (_) => BottomNavPosition(0),
        child: BottomNavigator(),
      ),
    );
  }
}
