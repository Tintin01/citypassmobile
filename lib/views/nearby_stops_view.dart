import 'package:citypass_mobile/blocs/transport_api_bloc/transport_api_bloc.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:provider/provider.dart';

import '../blocs/user_location_bloc/user_location_bloc.dart';
import '../store/bottom_navigation_state.dart';
import '../widgets/bottom_navigation_bar.dart';
import '../widgets/connection_error_card.dart';
import '../widgets/nearby_stops_list.dart';

class NearbyStopsView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _NearbyStopsViewState();
}

class _NearbyStopsViewState extends State<NearbyStopsView> {
  final location = Location();

  void getUserLocation(AsyncSnapshot snapshot) {
    userLocationBloc.updateUserLocation();
    Map<String, double> coords = snapshot.data;
    transportApiBloc.updateNearbyStopsModelList(coords);
  }

  Future<Map<String, double>> initialUserLocation() async {
    return await location.getLocation();
  }

  // @override
  // void dispose() {
  //   userLocationBloc.dispose();
  //   transportApiBloc.dispose();
  //   super.dispose();
  // }

  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: initialUserLocation(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              print(snapshot.error);
              return Padding(
                padding: const EdgeInsets.only(top: 40.0, bottom: 0.0),
                child: ConnectionErrorCard(),
              );
            }
            Map<String, double> coords = snapshot.data;
            transportApiBloc.updateNearbyStopsModelList(coords);
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: NearbyStopsCardList(userCoords: snapshot.data),
                ),
              ],
            );
          } else {
            return CircularProgressIndicator();
          }
        },
      ),
      bottomNavigationBar: ChangeNotifierProvider<BottomNavPosition>(
        builder: (_) => BottomNavPosition(2),
        child: BottomNavigator(),
      ),
    );
  }
}
