import 'package:flutter/material.dart';

class BottomNavPosition with ChangeNotifier {
  int _position;

  BottomNavPosition(this._position);

  getPosition() => _position;
  // setPosition(int position) => _position = position;

  setPosition(int position) {
    _position = position;
    notifyListeners();
  }
}
