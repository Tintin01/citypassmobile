import '../transport_api/schedule_data_model.dart';
import 'dart:convert';

class ScheduleDataList {
  List<ScheduleDataModel> sheduleData = [];

  ScheduleDataList({this.sheduleData});

  factory ScheduleDataList.fromJson(String parsedJson) {
    List<ScheduleDataModel> data = json.decode(parsedJson);

    return ScheduleDataList(
      sheduleData: data,
    );
  }
}