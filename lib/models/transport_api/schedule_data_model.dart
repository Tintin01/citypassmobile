class ScheduleDataModel {
  String arrivalTime;
  String departureTime;
  bool departs;
  bool arrives;
  Map vehicle;
  Map<String, dynamic> line;

  ScheduleDataModel({
    this.arrivalTime,
    this.departureTime,
    this.departs,
    this.arrives,
    this.vehicle,
    this.line,
  });

  factory ScheduleDataModel.fromJson(Map json) {
    return new ScheduleDataModel(
      arrivalTime: json['arrivalTime'],
      departureTime: json['departureTime'],
      departs: json['departs'],
      arrives: json['arrives'],
      vehicle: json['vehicle'],
      line: json['line'],
    );
  }
}
