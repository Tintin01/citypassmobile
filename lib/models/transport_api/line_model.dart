class LineModel {
  final String id;
  final String href;
  final Map<String, String> agency;
  final String name;
  final String mode;
  final String color;
  final String textColor;

  LineModel({this.id, this.href, this.agency, this.name, this.mode, this.color, this.textColor });

  factory LineModel.fromJson(Map<String, dynamic> json) {
    return new LineModel(
      id: json['id'], 
      href: json['href'], 
      agency: json['agency'], 
      name: json['name'], 
      mode: json['mode'], 
      color: json['color'], 
      textColor: json['textColor']
    );
  }
}
