class NearbyStopsModel {
  String id;
  String href;
  var agency;
  String name;
  Map<String, dynamic> geometry;
  List modes = [];

  NearbyStopsModel({
    this.id,
    this.href,
    this.agency,
    this.name,
    this.geometry,
    this.modes,
  });

  factory NearbyStopsModel.fromJson(Map json) {
    return new NearbyStopsModel(
      id: json['id'],
      href: json['href'],
      agency: json['agency'],
      name: json['name'],
      geometry: json['geometry'],
      modes: json['modes'],
    );
  }
}
