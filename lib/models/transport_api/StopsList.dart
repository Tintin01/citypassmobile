import 'package:citypass_mobile/models/transport_api/nearby_stops_model.dart';
import 'dart:convert';

class StopsList {
  List<NearbyStopsModel> stops = [];

  StopsList({this.stops});

  factory StopsList.fromJson(String parsedJson) {
    List<NearbyStopsModel> stops = json.decode(parsedJson);

    return StopsList(
      stops: stops,
    );
  }
}
