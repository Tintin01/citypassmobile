import 'dart:async';

import 'user_location_provider.dart';

class UserLocationBloc {
  final _userLocationController = StreamController.broadcast();
  final _provider = UserLocationProvider();

  Stream get getUserLocation => _userLocationController.stream;

  void updateUserLocation({latitude, longitude}) async {
    print('updateUserLocation running..');
    if (latitude == null || longitude == null) {
      var location = await _provider.setUserLocation();
      _userLocationController.sink.add(location);
    } else {
      var location = {"latitude": latitude, "longitude": longitude};
      _userLocationController.sink.add(location);
    }
  }

  void dispose() {
    _userLocationController.close();
  }
}

var userLocationBloc = UserLocationBloc();
