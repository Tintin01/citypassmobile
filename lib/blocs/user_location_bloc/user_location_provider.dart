import 'package:location/location.dart';
import 'package:geocoder/geocoder.dart';

class UserLocationProvider {
  Map<String, dynamic> _userLocation;

  final location = Location();

  Map<String, dynamic> get getUserLocation => _userLocation;

  Future<Map<String, double>> setUserLocation({latitude, longitude}) async {
    if (latitude == null || longitude == null) {
      _userLocation = await location.getLocation();
      return _userLocation;
    } else {
      _userLocation = {"latitude": latitude, "longitude": longitude};
      return _userLocation;
    }
  }

  Future<Address> getLocationFromCoords({lat, lng}) async {
    final coordinates = Coordinates(lat, lng);
    final addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    return addresses.first;
  }
}
