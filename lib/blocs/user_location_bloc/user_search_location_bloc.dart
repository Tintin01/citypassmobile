import 'dart:async';
import 'package:rxdart/rxdart.dart';
import 'user_location_provider.dart';
import '../../services/transport_api_config.dart';

class UserSearchLocationBloc {
  final _userFromLocationController = StreamController.broadcast();
  final _userToLocationController = StreamController.broadcast();
  // final _enableSubmit = BehaviorSubject();

  final _provider = UserLocationProvider();

  Stream get getUserFromLocation =>
      _userFromLocationController.stream;
  Stream get getUserToLocation =>
      _userToLocationController.stream;
  Stream<bool> get enableSubmit => Observable.combineLatest2(
          _userFromLocationController.stream, _userToLocationController.stream,
          (from, to) {
        print('ran function');
        return true;
      });

  void updateUserFromLocation({latitude, longitude}) async {
    var address =
        await _provider.getLocationFromCoords(lat: latitude, lng: longitude);
    var addressline = address.addressLine;
    var splitAddressline = addressline.split(",");
    var addressDetails = {
      "address": splitAddressline[0],
      "coords": {"lat": latitude, "long": longitude},
    };
    _userFromLocationController.sink.add(addressDetails);
  }

  void updateUserToLocation({latitude, longitude}) async {
    var address =
        await _provider.getLocationFromCoords(lat: latitude, lng: longitude);
    var addressline = address.addressLine;
    var splitAddressline = addressline.split(",");
    var addressDetails = {
      "address": splitAddressline[0],
      "coords": {"lat": latitude, "long": longitude},
    };
    _userToLocationController.sink.add(addressDetails);
  }

  void dispose() {
    _userFromLocationController.close();
    _userToLocationController.close();
  }
}

var userSearchLocationBloc = UserSearchLocationBloc();
