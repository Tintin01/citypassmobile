import 'dart:async';
import '../../services/firebase/fire_auth.dart';
import 'package:firebase_auth/firebase_auth.dart';
// import 'package:google_sign_in/google_sign_in.dart';

class CurrentUser {
  final StreamController<FirebaseUser> _currentUserController = StreamController.broadcast();

  Stream<FirebaseUser> get getCurrentUser => _currentUserController.stream;

  void setCurrentUser(Future<FirebaseUser> user) async {
    FirebaseUser currentUser = await user;
    _currentUserController.sink.add(currentUser);
  }

  void handleSignOutWithGoogle() async {
    handleGoogleSignOut();
  }

  void dispose() {
    _currentUserController.close();
  }
}

final CurrentUser currentUserBloc = CurrentUser();
