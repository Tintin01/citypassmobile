import 'dart:async';
import 'dart:convert';

import '../../models/transport_api/nearby_stops_model.dart';
import '../../services/transport_api_config.dart';

class TransportApiBloc {
  final _nearbyStopsModelListController = StreamController.broadcast();

  Stream get getNearbyStopsModelList => _nearbyStopsModelListController.stream;

  void updateNearbyStopsModelList(Map<String, double> coords) async {
    var stops = await TransportApiHandler.getNearbyStops(coords);

    Iterable list = json.decode(stops);
    List stopModels =
        list.map((model) => NearbyStopsModel.fromJson(model)).toList();
    _nearbyStopsModelListController.sink.add(stopModels);
  }

  void dispose() {
    _nearbyStopsModelListController.close();
  }
}

final TransportApiBloc transportApiBloc = TransportApiBloc();
