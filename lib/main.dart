import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import './services/transport_api_config.dart';
import './views/home_view.dart';
import './views/nearby_stops_view.dart';
import './views/trips_view.dart';
import './views/sign_in_view.dart';
import './views/splashscreen_view.dart';

void main() {
  runApp(App());

  TransportApiHandler.getBearerToken().then((res) {
    var token = json.decode(res.body);
    SharedPreferences.getInstance().then((res) {
      res.setString('token', token['access_token']);
    });
  });
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'City Pass',
      home: SplashScreenView(),
      theme: ThemeData(
        primaryColor: Colors.red[300],
      ),
      debugShowCheckedModeBanner: false,
      routes: {
        '/home': (context) => HomeView(),
        '/trips': (context) => TripsView(),
        '/nearby-stops': (context) => NearbyStopsView(),
        '/sign-in': (context) => SignInView(),
      },
    );
  }
}
