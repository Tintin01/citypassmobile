import 'package:flutter/material.dart';
import '../blocs/current_user/current_user_bloc.dart';
import '../services/firebase/fire_auth.dart';

class ViewTitle extends StatelessWidget {
  final String title;
  final String subtitle;
  final Widget utilityWidget;
  final BuildContext context;

  ViewTitle({
    @required this.title,
    @required this.subtitle,
    this.context,
    this.utilityWidget,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 0.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 32.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                title,
                style: TextStyle(
                    color: Colors.grey[700],
                    fontWeight: FontWeight.w700,
                    fontSize: 36.0),
              ),
              IconButton(
                icon: Icon(
                  Icons.account_circle,
                  color: Colors.red[300],
                ),
                splashColor: Colors.red[100],
                iconSize: 32.0,
                onPressed: _onBtnPressed,
              )
            ],
          ),
          SizedBox(height: 12.0),
          Text(
            subtitle,
            style: TextStyle(
                color: Colors.grey,
                fontWeight: FontWeight.w300,
                fontSize: 14.0),
          ),
          SizedBox(height: 16.0),
          utilityWidget != null ? utilityWidget : SizedBox(height: 0),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 24.0),
            child: Divider(
              color: Colors.grey[300],
            ),
          ),
        ],
      ),
    );
  }

  _onBtnPressed() {
    showModalBottomSheet(
      context: context,
      builder: (context) {
        return Container(
          color: Color(0xFF737373),
          child: Container(
            decoration: BoxDecoration(
              color: Theme.of(context).canvasColor,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(32),
                topRight: Radius.circular(32),
              ),
            ),
            child: _bottomSheetItems(context),
          ),
        );
      },
    );
  }
}

Widget _bottomSheetItems(BuildContext context) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: Text(
            "Account",
            style: TextStyle(
              fontSize: 24.0,
              color: Colors.grey[500],
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        FutureBuilder(
          future: getUser(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              print(snapshot.data);
              return ListTile(
                leading: CircleAvatar(
                  backgroundImage: NetworkImage(snapshot.data.photoUrl),
                ),
                title: Text(
                  "${snapshot.data.displayName}",
                  style: TextStyle(fontSize: 12.0),
                ),
                subtitle: Text(
                  "${snapshot.data.email}",
                  style: TextStyle(fontSize: 12.0, color: Colors.grey),
                ),
                onTap: () {},
              );
            } else {
              return CircularProgressIndicator();
            }
          },
        ),
        RaisedButton(
          elevation: 7.5,
          color: Colors.red[300],
          child: Text(
            'sign out',
            style: TextStyle(color: Colors.white),
          ),
          onPressed: () {
            handleGoogleSignOut().then((res) {
              Navigator.popAndPushNamed(context, '/sign-in');
            });
          },
        ),
        Divider(),
        SizedBox(
          height: 10.0,
        ),
        ListTile(
          leading: Icon(
            Icons.settings,
            size: 32,
            color: Colors.grey,
          ),
          title: Text(
            "settings",
            style: TextStyle(fontSize: 12.0),
          ),
          onTap: () {},
        ),
        SizedBox(
          height: 10.0,
        ),
        ListTile(
          leading: Icon(
            Icons.info,
            size: 32,
            color: Colors.grey,
          ),
          title: Text(
            "about",
            style: TextStyle(fontSize: 12.0),
          ),
          onTap: () {},
        ),
        SizedBox(
          height: 10.0,
        ),
        ListTile(
          leading: Icon(
            Icons.textsms,
            size: 32,
            color: Colors.grey,
          ),
          title: Text(
            "Connect",
            style: TextStyle(fontSize: 12.0),
          ),
          onTap: () {},
        ),
      ],
    ),
  );
}
