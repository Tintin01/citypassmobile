import 'package:flutter/material.dart';
import '../store/bottom_navigation_state.dart';
import 'package:provider/provider.dart';

class BottomNavigator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final position = Provider.of<BottomNavPosition>(context);

    return BottomNavigationBar(
      elevation: 0.0,
      currentIndex: position.getPosition(),
      selectedItemColor: Colors.red[300],
      unselectedItemColor: Colors.grey[700],
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.dashboard),
          title: Text("home"),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.compare_arrows),
          title: Text("trips"),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.near_me),
          title: Text("nearby stops"),
        ),
      ],
      onTap: (int index) {
        position.setPosition(index);
        switch (index) {
          case 0:
            Navigator.popAndPushNamed(context, '/home');
            break;
          case 1:
            Navigator.popAndPushNamed(context, '/trips');
            break;
          case 2:
            Navigator.popAndPushNamed(context, '/nearby-stops');
            break;
        }
        // if (index == 0) {
        //   Navigator.popAndPushNamed(context, '/home');
        // } else if (index == 1) {
        //   Navigator.popAndPushNamed(context, '/trips');
        // } else if (index == 2) {
        //   Navigator.popAndPushNamed(context, '/nearby-stops');
        // }
      },
    );
  }
}
