import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_webservice/places.dart';

import '../blocs/user_location_bloc/user_location_bloc.dart';

final kGoogleApiKey = "AIzaSyCPNcLRLKzVaX57zHizJaQN0ikBDvBzD-w";

GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: kGoogleApiKey);

enum type { asCard, asBtn }

class AutoCompleteLocationSearchBar extends StatefulWidget {
  final bool asBtn;
  final String text;

  AutoCompleteLocationSearchBar({@required this.asBtn, @required this.text});

  @override
  AutoCompleteLocationSearchBarState createState() =>
      new AutoCompleteLocationSearchBarState();
}

class AutoCompleteLocationSearchBarState
    extends State<AutoCompleteLocationSearchBar> {
  Future<Map<String, double>> displayPrediction(Prediction p) async {
    if (p != null) {
      PlacesDetailsResponse detail =
          await _places.getDetailsByPlaceId(p.placeId);

      var placeId = p.placeId;
      double lat = detail.result.geometry.location.lat;
      double lng = detail.result.geometry.location.lng;

      var address = await Geocoder.local.findAddressesFromQuery(p.description);

      print(address);
      print(placeId);

      print(lat);
      print(lng);

      userLocationBloc.updateUserLocation(latitude: lat, longitude: lng);

      Map<String, double> coords = {"longitude": lng, "latitude": lat};
      return coords;
    }
    return null;
  }

  Widget locationButton() {
    return RaisedButton.icon(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(32)),
      color: Colors.white,
      elevation: 6.0,
      splashColor: Colors.blueGrey,
      icon: Icon(
        Icons.search,
        color: Colors.red[300],
      ),
      label: Text(
        "search location",
        style: TextStyle(fontWeight: FontWeight.w400, color: Colors.red[300]),
      ),
      onPressed: () async {
        Prediction p = await PlacesAutocomplete.show(
          context: context,
          apiKey: kGoogleApiKey,
          mode: Mode.overlay,
          hint: "where are you?",
          language: "en",
          components: [
            new Component(Component.country, "za"),
          ],
        );
        displayPrediction(p);
      },
    );
  }

  Widget locationCard() {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(32)),
      elevation: 16.0,
      child: Container(
        height: 45.0,
        width: 70.0,
        child: RaisedButton.icon(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(32)),
          color: Colors.white,
          elevation: 0.0,
          splashColor: Colors.blueGrey,
          icon: Icon(
            Icons.search,
            color: Colors.red[300],
          ),
          label: Text(
            "${widget.text}",
            style:
                TextStyle(fontWeight: FontWeight.w400, color: Colors.red[300]),
          ),
          onPressed: () async {
            Prediction p = await PlacesAutocomplete.show(
              context: context,
              apiKey: kGoogleApiKey,
              mode: Mode.overlay,
              hint: "${widget.text}",
              language: "en",
              components: [
                new Component(Component.country, "za"),
              ],
            );
            displayPrediction(p);
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return widget.asBtn ? locationButton() : locationCard();
  }
}
