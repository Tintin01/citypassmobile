import 'package:flutter/material.dart';

class ConnectionErrorCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 16.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(24.0)),
      color: Colors.red[400],
      child: Container(
        padding: EdgeInsets.all(16.0),
        height: 150.0,
        width: 200.0,
        child: Center(
          child: Column(
            children: <Widget>[
              Text(
                "check your internet connection",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20.0,
                    fontWeight: FontWeight.w300),
              ),
              SizedBox(
                height: 32.0,
              ),
              Icon(
                Icons.network_check,
                color: Colors.white,
                size: 44.0,
              )
            ],
          ),
        ),
      ),
    );
  }
}
