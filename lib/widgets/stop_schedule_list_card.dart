import 'package:flutter/material.dart';
import '../models/transport_api/schedule_data_model.dart';

class StopScheduleListCard extends StatelessWidget {
  final ScheduleDataModel arrivalDataItem;
  final Map<String, dynamic> walkingDetails;

  StopScheduleListCard({@required this.arrivalDataItem, @required this.walkingDetails});
  
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 24.0),
      child: Card(
        elevation: 0.0,
        color: Colors.red[300],
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(22.0),
        ),
        child: Container(
          height: 166.0,
          width: 320.0,
          child: Padding(
            padding: EdgeInsets.only(top: 15.0, right: 15.0, left: 15.0, bottom: 0.0),
            child: Column(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Text(
                      '${arrivalDataItem.line['name']}',
                      style: TextStyle(
                          color: Colors.white70, fontWeight: FontWeight.bold),
                    ),
                    Divider(
                      color: Colors.white70,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'arrives: ${arrivalDataItem.arrivalTime}',
                      style: TextStyle(
                          color: Colors.white70, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        OutlineButton.icon(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(32.0),
                          ),
                          borderSide: BorderSide(color: Colors.white70),
                          color: Colors.white70,
                          icon: Icon(
                            Icons.chat_bubble_outline,
                            color: Colors.white,
                          ),
                          label: Text(
                            "message board",
                            style: TextStyle(color: Colors.white),
                          ),
                          onPressed: () {},
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
