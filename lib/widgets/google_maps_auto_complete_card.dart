import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import '../blocs/user_location_bloc/user_search_location_bloc.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_webservice/places.dart';

final kGoogleApiKey = "AIzaSyCPNcLRLKzVaX57zHizJaQN0ikBDvBzD-w";

GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: kGoogleApiKey);

class AutoCompleteLocationSearchBar extends StatefulWidget {
  final bool isFromLocation;
  final String text;

  AutoCompleteLocationSearchBar(
      {@required this.text, @required this.isFromLocation});

  @override
  AutoCompleteLocationSearchBarState createState() =>
      new AutoCompleteLocationSearchBarState();
}

class AutoCompleteLocationSearchBarState
    extends State<AutoCompleteLocationSearchBar> {
  Future<Map<String, double>> displayPrediction(Prediction p) async {
    if (p != null) {
      PlacesDetailsResponse detail =
          await _places.getDetailsByPlaceId(p.placeId);

      var placeId = p.placeId;
      double lat = detail.result.geometry.location.lat;
      double lng = detail.result.geometry.location.lng;

      List<Address> address =
          await Geocoder.local.findAddressesFromQuery(p.description);

      print(address.first.addressLine);

      if (widget.isFromLocation) {
        userSearchLocationBloc.updateUserFromLocation(
            latitude: lat, longitude: lng);
      } else {
        userSearchLocationBloc.updateUserToLocation(
            latitude: lat, longitude: lng);
      }

      Map<String, double> coords = {"longitude": lng, "latitude": lat};
      return coords;
    }
    return null;
  }

  // getLocationFromCoords({@required lat, @required lng}) async {
  //   final coordinates = Coordinates(lat, lng);
  //   final addresses =
  //       await Geocoder.local.findAddressesFromCoordinates(coordinates);
  //   var first = addresses.first;
  //   print(first.addressLine);
  // }

  Widget locationCard() {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(32)),
      elevation: 4.0,
      child: Container(
        height: 45.0,
        width: 320.0,
        child: StreamBuilder(
          stream: widget.isFromLocation
              ? userSearchLocationBloc.getUserFromLocation
              : userSearchLocationBloc.getUserToLocation,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            return RaisedButton.icon(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(32),
              ),
              color: Colors.white,
              elevation: 0.0,
              splashColor: Colors.blueGrey,
              icon: Icon(
                Icons.place,
                color: Colors.red[300],
              ),
              label: snapshot.data != null
                  ? Text(
                      "${snapshot.data["address"]}",
                      style: TextStyle(
                          fontWeight: FontWeight.w400, color: Colors.red[300]),
                    )
                  : Text(
                      "${widget.text}",
                      style: TextStyle(
                          fontWeight: FontWeight.w400, color: Colors.red[300]),
                    ),
              onPressed: () async {
                Prediction p = await PlacesAutocomplete.show(
                  context: context,
                  apiKey: kGoogleApiKey,
                  mode: Mode.fullscreen,
                  hint: "${widget.text}",
                  language: "en",
                  components: [
                    new Component(Component.country, "za"),
                  ],
                );
                displayPrediction(p);
              },
            );
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return locationCard();
  }
}
