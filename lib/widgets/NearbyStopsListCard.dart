import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' show get;

import '../models/transport_api/nearby_stops_model.dart';
import '../views/bus_details_view.dart';

class NearbyStopsListCard extends StatefulWidget {
  final Map<String, double> userCoords;
  final NearbyStopsModel stop;

  NearbyStopsListCard({
    @required this.userCoords,
    @required this.stop,
  });

  @override
  _NearbyStopsListCardState createState() => _NearbyStopsListCardState();
}

class _NearbyStopsListCardState extends State<NearbyStopsListCard> {
  var walkingDetails;

  Future<Map<String, dynamic>> getWalkingDetails() async {
    var response = await get(
        'https://route.api.here.com/routing/7.2/calculateroute.json?app_id=ZyWWcMmfvVPGW6QlXaYF&app_code=I7Aa4U5hcpcKhecQiGj7TA&waypoint0=geo!${widget.userCoords['latitude']},${widget.userCoords['longitude']}&waypoint1=geo!${widget.stop.geometry['coordinates'][1]},${widget.stop.geometry['coordinates'][0]}&mode=fastest;pedestrian');

    var body = json.decode(response.body);

    String distance =
        (body['response']['route'][0]['summary']['distance'] / 1000)
            .toStringAsFixed(2);

    String time = (body['response']['route'][0]['summary']['baseTime'] / 60)
        .toStringAsFixed(2);

    walkingDetails = {"distance": distance, "time": time};
    return {"distance": distance, "time": time};
  }

  Widget cardSummaryData(Map<String, dynamic> data) {
    return Column(
      children: <Widget>[
        ListTile(
          leading: Icon(
            Icons.navigation,
            color: Colors.white70,
          ),
          title: Text(
            "Distance: ${data['distance']} KM",
            style: TextStyle(color: Colors.white70),
          ),
        ),
        ListTile(
          leading: Icon(
            Icons.directions_walk,
            color: Colors.white70,
          ),
          title: Text(
            "Walking time: ${data['time']}",
            style: TextStyle(color: Colors.white70),
          ),
        ),
      ],
    );
  }

  void _pushDataToDetailsView(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => BusDetailsView(
              stop: widget.stop,
              userCoords: widget.userCoords,
              walkingDetails: walkingDetails,
            ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Card(
        elevation: 0.0,
        color: Colors.red[300],
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(22.0)),
        child: Container(
          height: 320.0,
          child: Padding(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: <Widget>[
                Text(
                  "${widget.stop.name}",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w800,
                    fontSize: 16.0,
                  ),
                ),
                SizedBox(height: 12.0),
                Divider(
                  height: 12.0,
                  color: Colors.white,
                ),
                SizedBox(height: 12.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "${widget.stop.agency['name']}",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w900,
                          fontSize: 16.0),
                    ),
                  ],
                ),
                SizedBox(height: 16.0),
                FutureBuilder(
                  future: getWalkingDetails(),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                      return cardSummaryData(snapshot.data);
                    } else {
                      return SizedBox(
                        height: 100.0,
                        child: Center(
                          child: CircularProgressIndicator(
                            valueColor: new AlwaysStoppedAnimation<Color>(
                                Colors.red[100]),
                          ),
                        ),
                      );
                    }
                  },
                ),
                SizedBox(height: 24.0),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      OutlineButton.icon(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(32.0),
                        ),
                        borderSide: BorderSide(color: Colors.white70),
                        color: Colors.white70,
                        icon: Icon(
                          Icons.star_border,
                          color: Colors.white,
                        ),
                        label: Text(
                          "favorite",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {},
                      ),
                      SizedBox(
                        width: 16.0,
                      ),
                      OutlineButton.icon(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(32.0),
                        ),
                        borderSide: BorderSide(color: Colors.white70),
                        color: Colors.white70,
                        icon: Icon(
                          Icons.chevron_right,
                          color: Colors.white,
                        ),
                        label: Text(
                          "more",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          _pushDataToDetailsView(context);
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
