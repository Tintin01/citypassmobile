import 'package:citypass_mobile/models/transport_api/nearby_stops_model.dart';
import 'package:flutter/material.dart';

import '../blocs/transport_api_bloc/transport_api_bloc.dart';
import '../models/transport_api/nearby_stops_model.dart';
import '../widgets/NearbyStopsListCard.dart';
import '../widgets/google_maps_autocomplete_searchbar.dart';
import '../widgets/view_title.dart';

class NearbyStopsCardList extends StatefulWidget {
  final Map<String, double> userCoords;

  NearbyStopsCardList({this.userCoords});

  @override
  State<StatefulWidget> createState() => _NearbyStopsCardListState();
}

class _NearbyStopsCardListState extends State<NearbyStopsCardList> {
  // @override
  // void dispose() {
  //   transportApiBloc.dispose();
  //   super.dispose();
  // }

  List stopCards = [];

  Widget renderStopCards(List stopCardList) {
    List<Widget> modeledCards = [];

    for (var item in stopCardList) {
      modeledCards.add(
        NearbyStopsListCard(
          userCoords: widget.userCoords,
          stop: item,
        ),
      );
    }

    return Column(children: modeledCards);
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<Object>(
      stream: transportApiBloc.getNearbyStopsModelList,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Padding(
          padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 8.0),
          child: ListView(
            children: <Widget>[
              ViewTitle(
                title: "Nearby Stops",
                subtitle: "Find Stops Near You.",
                context: context,
              ),
              SizedBox(height: 16.0),
              Center(
                child: AutoCompleteLocationSearchBar(
                  asBtn: true,
                  text: "search location",
                ),
              ),
              SizedBox(height: 24.0),
              StreamBuilder(
                stream: transportApiBloc.getNearbyStopsModelList,
                builder: (context, snapshot) {
                  if (snapshot.data != null) {
                    for (NearbyStopsModel item in snapshot.data) {
                      stopCards.add(item);
                    }
                    return renderStopCards(stopCards);
                  } else {
                    return Padding(
                      padding: const EdgeInsets.only(top: 40.0),
                      child: Card(
                        elevation: 16.0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(24.0),
                        ),
                        color: Colors.red[400],
                        child: Container(
                          padding: EdgeInsets.all(16.0),
                          height: 200.0,
                          width: 200.0,
                          child: Center(
                            child: Column(
                              children: <Widget>[
                                Text(
                                  "beep boop beep...",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.w300),
                                ),
                                SizedBox(
                                  height: 16.0,
                                ),
                                Text(
                                  "🤖",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 40.0,
                                      fontWeight: FontWeight.w300),
                                ),
                                SizedBox(
                                  height: 32.0,
                                ),
                                CircularProgressIndicator(
                                  backgroundColor: Colors.red[400],
                                  valueColor: new AlwaysStoppedAnimation<Color>(
                                    Colors.red[100],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  }
                },
              ),
            ],
          ),
        );
      },
    );
  }
}
