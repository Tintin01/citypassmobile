import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class TransportApiHandler {
  static getBearerToken() async {
    var response = await http.post(
      "https://identity.whereismytransport.com/connect/token",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json',
        'cache-control': 'no-cache'
      },
      body: {
        'client_id': '14e4d115-187e-4b43-80be-021d03efd7b6',
        'client_secret': '8YqUUPqK9QTAF2FFNeXNdX4hA+tiJddgmM6WxrR9LFM=',
        'grant_type': 'client_credentials',
        // 'scope': 'transportapi%3Aall',
      },
    );
    return response;
  }

  static getJourneys(Map<String, double> coords) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    var response = await http.post(
      'https://platform.whereismytransport.com/api/journeys',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json',
        'cache-control': 'no-cache',
        'Authorization': "Bearer ${prefs.get('token')}"
      },
      body: {
        'client_id': '14e4d115-187e-4b43-80be-021d03efd7b6',
        'client_secret': '8YqUUPqK9QTAF2FFNeXNdX4hA+tiJddgmM6WxrR9LFM=',
        'grant_type': 'client_credentials',
      },
    );

    return response;
  }

  static getStopSchedule({String stopId}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    try {
      var res = await http.get(
        'https://platform.whereismytransport.com/api/stops/$stopId/timetables?',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Accept': 'application/json',
          'Authorization': "Bearer ${prefs.get('token')}"
        },
      );

      return res.body;
    } catch (e) {
      print(e);
    }
  }

  static getNearbyStops(Map<String, double> coords) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    try {
      var response = await http.get(
        'https://platform.whereismytransport.com/api/stops?&radius=750&point=${coords['latitude']},${coords['longitude']}',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Accept': 'application/json',
          'Authorization': "Bearer ${prefs.get('token')}"
        },
      );

      return response.body;
    } catch (e) {
      print(e);
    }
  }
}
