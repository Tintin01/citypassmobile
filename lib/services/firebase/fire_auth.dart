// import 'package:citypass_mobile/blocs/current_user/current_user_bloc.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import '../../blocs/current_user/current_user_bloc.dart';

final GoogleSignIn _googleSignIn = GoogleSignIn();
final FirebaseAuth _auth = FirebaseAuth.instance;

Future<FirebaseUser> handleGoogleSignIn() async {
  print('google sign in running...');
  final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
  final GoogleSignInAuthentication googleAuth = await googleUser.authentication;

  final AuthCredential credential = GoogleAuthProvider.getCredential(
    accessToken: googleAuth.accessToken,
    idToken: googleAuth.idToken,
  );

  final FirebaseUser user = await _auth.signInWithCredential(credential);
  assert(user.email != null);
  assert(user.displayName != null);
  assert(!user.isAnonymous);
  assert(await user.getIdToken() != null);

  Future<FirebaseUser> _currentUser = _auth.currentUser();
  currentUserBloc.setCurrentUser(_currentUser);
  return _currentUser;
}

Future<FirebaseUser> getUser() async {
  return await _auth.currentUser();
}

Future<bool> handleGoogleSignOut() async {
  await _googleSignIn.signOut();
  await _auth.signOut();
  return true;
}
